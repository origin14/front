import swal from "sweetalert2";

const Toast = swal.mixin({
  toast: true,
  position: 'bottom-start',
  showConfirmButton: false,
  showCloseButton: true,
  timer: 5000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', swal.stopTimer)
    toast.addEventListener('mouseleave', swal.resumeTimer)
  }
})

function toast(icon, message, title = undefined) {
  Toast.fire({
    icon: icon,
    text: message,
    title: title
  })
}
export default (ctx, inject) => {
  inject('toast', toast)
}
