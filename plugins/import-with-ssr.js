import Vue from 'vue'
import money from 'v-money'

export default () => {
  Vue.use(money, {precision: 2})
};
