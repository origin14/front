import api from '~/api/default'


const API_URL = 'http://localhost:8081'

export default (ctx, inject) => {
  //Configurando Axios
  setupInterceptors(ctx.$axios, ctx)

  //Injecões da api de autenticacao
  const apiAxios = api(ctx.$axios)
  inject('financial', apiAxios(API_URL + '/financial'))

}

let setupInterceptors = (axios, ctx) => {
  let actions = {
    400: (error) => {
      ctx.$toast('error', 'Erro', error)
    },
    401: () => {
      ctx.$toast('error', 'Usuário e senha não encontrado', 'Por favor tente novamente')
    },
    403: () => {
      ctx.$toast('error', 'Login necessário', 'Por favor faça o login novamente')
    },
    404: ({ data }) => {
      ctx.$toast('error', 'Erro', data.message)
    },
    'network': () => {
      ctx.$toast('error', 'Erro de conexão', 'Recarregue a página ou tente novamente mais tarde')
    },
    'default': (code) => {
      ctx.$toast('error', 'Erro' + code, 'Entre em contato com o suporte e informe um erro')
    }
  }

  axios.interceptors.response.use((response) => {
    const { status } = response

    if (status !== 200) {

      try {
        actions[status](response)
      } catch (e) {
        actions.default(status)
      }

      return Promise.reject(response)
    }

    return response
  }, (error) => {
    try {
      const { status } = error.response
      actions[status](error.response)
    } catch (e) {
      actions.network()
    }

    return Promise.reject(error)
  })
}
