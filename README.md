# Origin Take Home Assigment (Front)

This project was developed using Nuxt (Vue.js). 


## How to run the project ?

### Requirements
* Node 14.16.0

### Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

## Structure

This project has the following structure:

- api
  - The api directory contains the files that design the api REST pattern used by axios
- assets
  - The assets directory contains your uncompiled assets
- components
  - The components directory contains the vue.js components. They are separate by subfolders, each one refer their respective use. They are embbebed inside the page files
- layouts
  - The parent of all components and pages, this directory contains the roots components
- pages
  - This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.
- plugins
  - The plugins directory contains JavaScript plugins that you want inject in the context
- static
  - This directory contains your static files. Each file inside this directory is mapped to `/`.
